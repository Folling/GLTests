#pragma once


#include <vector>

#include <GLFW/glfw3.h>

#include "../include/glad/glad.h"

#include "shader.h"

class ShaderProgram {
public:
    ShaderProgram();
    void use();
    template<unsigned int K>
    void attachShader(Shader<K>& toAttach);
    void attachShader(unsigned int& shaderID);
    unsigned int& getID();
private:
    unsigned int ID;
    std::vector<unsigned int> shaderIDs;
};

ShaderProgram::ShaderProgram() {
    ID = glCreateProgram();
    shaderIDs = std::vector<unsigned int>{};
}

void ShaderProgram::use() {
    
    if(shaderIDs.size() == 0){
        std::cerr << "Using program with 0 shaders attached.\n";
    }
    
    glLinkProgram(ID);
    
    int success;
    char errorInfo[descriptiveErrorLength];
    
    glGetProgramiv(ID, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(ID, 512, NULL, errorInfo);
        std::cerr << "Unable to create shader program!\n" << errorInfo << '\n';
    }
    
    glUseProgram(ID);
    
    for(unsigned int& id : shaderIDs){
        glDeleteShader(id);
    }
    
}

template<unsigned int K>
void ShaderProgram::attachShader(Shader<K>& toAttach) {
    attachShader(toAttach.getID());
}

void ShaderProgram::attachShader(unsigned int& shaderID) {
    glAttachShader(ID, shaderID);
    shaderIDs.push_back(shaderID);
}

unsigned int& ShaderProgram::getID() {
    return ID;
}

