#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <sys/stat.h>

#include <GLFW/glfw3.h>

#include "../include/glad/glad.h"

const unsigned int descriptiveErrorLength = 512;

template<unsigned int K>
class Shader {
public:
    explicit Shader(const char* path);
public:
    unsigned int& getID();
    unsigned int getProgramID() const;
    const int& getType();
public:
    template<typename T>
    void setUniform(const std::string& name, T value) const;
private:
    inline bool fileExists(const char* path){
        struct stat buffer {};
        return (stat (path, &buffer) == 0);
    }
private:
    unsigned int ID;
    unsigned int programID;
private:
    int type = K;
    std::string content;
    const std::string filePath;
    std::string vertexCode;
};

template<unsigned int K>
Shader<K>::Shader(const char* path):
        filePath(path)
{
    static_assert(
            K == GL_VERTEX_SHADER
            || K == GL_FRAGMENT_SHADER
            || K == GL_GEOMETRY_SHADER, "The specified Shader type doesn't exist."
    );
    
    if(!fileExists(path)){
        std::cerr << "Path " << path << " doesn't exist.\n";
    }
    
    std::ifstream reader;
    
    reader.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    
    try{
        reader.open(path);
        content = std::string(
                std::istreambuf_iterator<char>(reader),
                std::istreambuf_iterator<char>()
        );
        reader.close();
    } catch(std::ifstream::failure& e){
        std::cerr
                << "An error occured trying to read the file " << path  << '\n'
                << "\n\n" << e.what() << std::endl;
    } catch(...){
        std::cerr
                << "Unidentified error occured trying to read the file " << path << std::endl;
    }
    
    int success;
    
    ID = glCreateShader(K);
    
    const char* shaderContent = content.data();
    
    char errorInfo[descriptiveErrorLength];
    glShaderSource(ID, 1, &shaderContent, NULL);
    glCompileShader(ID);
    glGetShaderiv(ID, GL_COMPILE_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(ID, descriptiveErrorLength, NULL, errorInfo);
        std::cerr << "Error compiling shader: " << errorInfo << std::endl;
        getchar();
    }
}

template<unsigned int K>
template<typename T>
void Shader<K>::setUniform(const std::string& name, T value) const {
    if(std::is_same<T, bool>::value || std::is_same<T, int>::value){
        glUniform1i(glGetUniformLocation(programID, name.c_str()), static_cast<int>(value));
    } else if (std::is_same<T, float>::value){
        glUniform1f(glGetUniformLocation(programID, name.c_str()), value);
    }
};

template<unsigned int K>
unsigned int& Shader<K>::getID() {
    return ID;
}

template<unsigned int K>
const int& Shader<K>::getType() {
    return type;
}

template<unsigned int K>
unsigned int Shader<K>::getProgramID() const {
    return programID;
}