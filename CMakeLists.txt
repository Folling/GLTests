cmake_minimum_required(VERSION 3.9)
project(OpenGLTests)

set(CMAKE_CXX_STANDARD 17)

find_package(OpenGL REQUIRED)
find_package(PkgConfig REQUIRED)

pkg_search_module(GLFW REQUIRED glfw3)

add_subdirectory(Shader)

add_executable(OpenGLTests main.cpp include/glad.c)

target_compile_definitions(OpenGLTests PUBLIC GLFW_INCLUDE_NONE=1)

include_directories(${OPEN_GL_INCLUDE_DIRS} ${GLFW_INCLUDE_DIRS})

target_link_libraries(OpenGLTests ${OPENGL_LIBRARIES} ${GLFW_LIBRARIES})

# glad
set(GLAD_DIR "include")
add_library("glad" "${GLAD_DIR}/glad.c")
target_include_directories("glad" PRIVATE "${GLAD_DIR}/include")
target_include_directories(${PROJECT_NAME} PRIVATE "${GLAD_DIR}/include")
target_link_libraries(${PROJECT_NAME} "glad" "${CMAKE_DL_LIBS}")