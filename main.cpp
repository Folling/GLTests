#include <iostream>
#include <random>

#include <GLFW/glfw3.h>

#include "include/glad/glad.h"

#include "Shader/shaderProgram.h"
#include "Shader/shader.h"

typedef unsigned int ID;

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void close_window_on_escape(GLFWwindow* window, int key, int scancode, int action, int mods){
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}

GLFWwindow* setWindow(){
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    GLFWwindow* window = glfwCreateWindow(800, 600, "OpenGLTest", NULL, NULL);
    if(!window){
        std::cerr << "Failed to create GLFW Window\n";
        glfwTerminate();
    }
    return window;
}

int main() {
    
    glfwInit();
    auto window = setWindow();
    if(!window) return -1;
    glfwMakeContextCurrent(window);
    
    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
        std::cerr << "Failed to initialise GLAD\n";
        return -1;
    }
    
    ShaderProgram program;
    Shader<GL_VERTEX_SHADER> vertexShader{"/home/folling/CLionProjects/OpenGLTests/Shaders/vertexshader1.vs"};
    Shader<GL_FRAGMENT_SHADER> fragShader{"/home/folling/CLionProjects/OpenGLTests/Shaders/fragshader1.fs"};
    
    program.attachShader(vertexShader);
    program.attachShader(fragShader);
    
    program.use();
    
    float firstVertices[] = {
            0.5f, 0.0f, 0.0f,
            -0.5f, 0.0f, 0.0f,
            0.0f, 0.5f, 0.0f
    };
    
    float secondVertices[] = {
            -0.5f, 0.8f, 0.0f,
            1.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f
    };
    
    ID VBO;

    ID VAO;
    
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    
    glBindVertexArray(VAO);
    
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(firstVertices), firstVertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    
    glClearColor(0.0f, 0.2f, 0.5f, 1.0f);
    
    while(!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);

        // update shader uniform
        float timeValue = glfwGetTime();
        float greenValue = sin(timeValue) + 0.5f;
        int vertexColorLocation = glGetUniformLocation(program.getID(), "ourColor");
        glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);
        
        glDrawArrays(GL_TRIANGLES, 0, 3);
        
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    
    glfwTerminate();
}